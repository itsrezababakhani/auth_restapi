const express = require("express");
const brandsController = require("./controller");
const router = express.Router();
router.post("/get", brandsController.getBrands);
module.exports = router;
