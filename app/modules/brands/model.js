const connection = require("../../../database/connection/mysql");
exports.getBrands = async () => {
  const db = await connection();
  const [result, fields] = await db.query("SELECT * FROM `brands`");
  return result;
};
