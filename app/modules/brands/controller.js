const brandModel = require("./model");
exports.getBrands = async (req, res) => {
  const brands = await brandModel.getBrands();
  res.send({
    success: true,
    brands: brands
  });
};
