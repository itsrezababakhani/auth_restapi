const portfolioModel = require("./model");
exports.trendsPortfolio = async (req, res) => {
  const trendsPortfolio = await portfolioModel.getTrendsPortfolio();
  res.send({
    posts: trendsPortfolio
  });
};

exports.portfolio = async (req, res) => {
  const portfolio = await portfolioModel.getTrendsPortfolio();

  res.send({
    posts: portfolio
  });
};

exports.getPortfolioByUID = async (req, res) => {
  const { id } = req.body;
  const data = await portfolioModel.getPortfolioByID(id);
  if (!data) {
    res.send({
      success: false,
      message: "اطلاعاتی وجود ندارد"
    });
  }
  res.send({
    success: true,
    data: data
  });
};
