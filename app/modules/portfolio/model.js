const connection = require("../../../database/connection/mysql");
exports.getTrendsPortfolio = async () => {
  const db = await connection();
  const [posts, fields] = await db.query(
    "SELECT * FROM portfolio WHERE trends = 1 LIMIT 3"
  );
  return posts;
};

exports.getPortfolio = async () => {
  const db = await connection();
  const [posts, fields] = await db.query("SELECT * FROM `portfolio`");
  return posts;
};

exports.getPortfolioByID = async id => {
  const db = await connection();
  const [data, fields] = await db.query(
    "SELECT * FROM `portfolio` WHERE id = ?",
    id
  );
  if (data.length > 0) {
    return data;
  }
  return false;
};
