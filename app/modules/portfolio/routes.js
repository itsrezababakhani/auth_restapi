const express = require("express");
const portfolioController = require("./controller");

const router = express.Router();

router.get("/trends", portfolioController.trendsPortfolio);
router.get("/", portfolioController.portfolio);
router.post("/uid", portfolioController.getPortfolioByUID);
module.exports = router;
