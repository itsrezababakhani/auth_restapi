const tutorialsModel = require("./model");
exports.trendsTutorial = async (req, res) => {
  const trendsTutorial = await tutorialsModel.getTrendsTutorial();
  res.send({
    tutorials: trendsTutorial
  });
};

exports.getTutorialByID = async (req, res) => {
  const { id } = req.body;

  const data = await tutorialsModel.getTutorialByID(id);
  if (!data) {
    res.send({
      success: false,
      message: "اطلاعاتی یافت نشد"
    });
  }
  res.send({
    success: true,
    data
  });
};
