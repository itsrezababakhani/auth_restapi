const express = require("express");
const tutorialsController = require("./controller");
const router = express.Router();
router.get("/trends", tutorialsController.trendsTutorial);
router.post("/id", tutorialsController.getTutorialByID);
module.exports = router;
