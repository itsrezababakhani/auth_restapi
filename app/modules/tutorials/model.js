const connection = require("../../../database/connection/mysql");
exports.getTrendsTutorial = async () => {
  const db = await connection();
  const [tutorials, fields] = await db.query(
    "SELECT * FROM `tutorials` WHERE trends = 1 LIMIT 3"
  );
  return tutorials;
};

exports.getTutorialByID = async id => {
  const db = await connection();
  const [data, fields] = await db.query(
    "SELECT * FROM `tutorials` WHERE id = ?",
    id
  );
  if (data.length > 0) {
    return data;
  }
  return false;
};
