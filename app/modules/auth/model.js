const connection = require("../../../database/connection/mysql");
exports.loginByPhoneNumber = async (email, password) => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `users` WHERE email = ? AND password = ?",
    [email, password]
  );
  if (result.length > 0) {
    return result[0];
  }
  return false;
};

exports.createUserByPhoneNumber = async user => {
  const db = await connection();
  const [result, fields] = await db.query("INSERT INTO `users` SET ?", user);
  return result;
};

exports.findUserByEmail = async email => {
  const db = await connection();
  const [result, fields] = await db.query(
    "SELECT * FROM `users` WHERE email = ?",
    email
  );
  if (result.length > 0) {
    return true;
  }
  return false;
};
