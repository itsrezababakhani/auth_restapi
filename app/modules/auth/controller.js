const authModel = require("./model");
const tokenService = require("./token");
exports.login = async (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    res.status(422).send({
      success: false,
      message: "اطلاعات مورد نیاز برای ورود را تکمیل کنید"
    });
  }

  const userLogin = await authModel.loginByPhoneNumber(email, password);

  if (!userLogin) {
    res.status(401).send({
      success: false,
      message: "کاربری با این مشخصات یافت نشد."
    });
  }

  const token = await tokenService.createSign({
    uid: userLogin.id,
    username: userLogin.username
  });

  res.send({
    success: true,
    message: "ورود با موفقیت انجام شد",
    username: userLogin.phone,
    token
  });
};

exports.register = async (req, res) => {
  const { phone, email, password } = req.body;

  if (!phone || !email || !password) {
    res.status(422).send({
      success: false,
      message: "اطلاعات مورد نیاز برای عضویت را تکمیل کنید"
    });
  }

  const userData = {
    phone,
    email,
    password
  };
  const checkExistUser = await authModel.findUserByEmail(email);
  if (checkExistUser) {
    return res.send({
      success: false,
      message: "کاربر با این مشخصات در سیستم ثبت نام کرده است"
    });
  }
  const user = await authModel.createUserByPhoneNumber(userData);

  if (user.affectedRows > 0) {
    const token = await tokenService.createSign({
      uid: user.insertId,
      username: userData.email
    });

    res.send({
      success: true,
      message: "عضویت با موفقیت انجام شد",
      token
    });
  }
};
