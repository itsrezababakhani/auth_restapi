const tokenService = require("../modules/auth/token");
exports.auth = (req, res, next) => {
  const token = tokenService.getToken(req);
  if (!token) {
    res.status(401).send({
      success: false,
      message: "شما قادر به دسترسی به این صفحه نمی باشید"
    });
  }

  const payload = tokenService.verifyToken(token);
  if (!payload) {
    return res.status(401).send({
      success: false,
      message: "شما قادر به دسترسی به این صفحه نمی باشید"
    });
  }

  return next();
};
