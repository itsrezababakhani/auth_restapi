const portfolioRoutes = require("./modules/portfolio/routes");
const tutorialRoutes = require("./modules/tutorials/routes");
const authRoutes = require("./modules/auth/routes");
const userRoutes = require("./modules/user/routes");
const brandsRoutes = require("./modules/brands/routes");
const { auth } = require("./middlewares/authmiddleware");

exports.router = app => {
  app.use("/api/v1/portfolio", portfolioRoutes);
  app.use("/api/v1/tutorial", tutorialRoutes);
  app.use("/api/v1/auth", authRoutes);
  app.use("/api/v1/user", userRoutes);
  app.use("/api/v1/brands", brandsRoutes);
};
